#include <stdlib.h>
#include <sqlite3.h>

int create_table(sqlite3 *db);
int save_note(sqlite3 *db, char *note, char *inserted_uuid);
int get_note(sqlite3 *db, char* uuid, char *buf, size_t buf_size);
int get_all_notes(sqlite3 *db, char* buf, size_t buf_size);
