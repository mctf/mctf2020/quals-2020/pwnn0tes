# pwnn0tes

Build

```sh
gcc server.c database.c -o server -lsqlite3 -luuid -fno-stack-protector
```

# [решение](solution.py)

``` c

// внутри функции get_notes файла server.c
int permit = 0;
char password[UUID_LEN];
read(client_socket, password, 2048);
/*
примерный вид стека, учитывая, что он расстет вверх, а адреса вниз
@@@@@@...@@@@@xxxxxxxxxxxx...xxxxxxxxxxxxxxxxxxx\x00\x00\x00\x00
password      указатели на сообщения ошибок     permit
*/
```