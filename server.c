#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>

#include "database.h"

// Settings
#define INPUT_BUF_SIZE 100
#define UUID_LEN 37
#define DB_FILENAME "notes.db"
#define TRUE 1
#define FALSE 0
#define ALL_NOTES_BUF_SIZE 10000

int client_socket = 0;

void get_notes()
{
    // ------------------
    // Getting flags here
    // ------------------
    int permit = 0;
    
    char* require_password = "Enter password\n> ";
    write(client_socket, require_password, strlen(require_password));
    
    char password[UUID_LEN];
    read(client_socket, password, 2048);
    int min = strlen(password);
    if (UUID_LEN < min)
    {
        min = UUID_LEN;
    }
    password[min - 1] = 0;
    
    char* pass = getenv("PASS");
    
    if (!strcmp(password, pass))
    {
        permit = 42;
    }
    else
    {
        char* bad_pass = "Password is incorrect\n";
        write(client_socket, bad_pass, strlen(bad_pass));
    }
    
    if (permit == 42)
    {
        sqlite3 *db;
        sqlite3_stmt *res;
        int rc = sqlite3_open(DB_FILENAME, &db);
        if (rc != SQLITE_OK)
        {
            fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
            sqlite3_close(db);
            return;
        }
        create_table(db);
        char* buf = calloc(ALL_NOTES_BUF_SIZE, sizeof(char));
        get_all_notes(db, buf, ALL_NOTES_BUF_SIZE);
        write(client_socket, buf, strlen(buf));
        write(client_socket, "\n", 1);
        free(buf);
    }
}

void save(sqlite3 *db)
{
    char* save_message = "Input your note below:\n";
    write(client_socket, save_message, strlen(save_message));

    char ch[INPUT_BUF_SIZE] = {0};
    read(client_socket, ch, INPUT_BUF_SIZE);
    char* uuid = calloc(UUID_LEN, sizeof(char));
    save_note(db, ch, uuid);

    write(client_socket, uuid, UUID_LEN);
    write(client_socket, "\n", 1);
    free(uuid);
}

void read_note(sqlite3 *db, int rc)
{
    char* save_message = "Input note UUID below:\n";
    write(client_socket, save_message, strlen(save_message));

    char ch[INPUT_BUF_SIZE] = {0};
    read(client_socket, ch, INPUT_BUF_SIZE);
    char* uuid = calloc(UUID_LEN, sizeof(char));
    strncpy(uuid, ch, UUID_LEN - 1);

    char note[100];
    rc = get_note(db, uuid, (char *)&note, 100);
    if (!rc) 
    {
        write(client_socket, note, strlen(note));
    } 
    else
    {
        char* error = "Not found!\n";
        write(client_socket, error, strlen(error));
    }
    free(uuid);
}

void dialog() {
    sqlite3 *db;
    sqlite3_stmt *res;
    int rc = sqlite3_open(DB_FILENAME, &db);
    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return;
    }
    create_table(db);

    char* ch = calloc(INPUT_BUF_SIZE, sizeof(char));
    printf("Get connection\n");
    char* welcome_message = "Welcome to the note storage.\n\n Choose action:\n 1) read note\n 2) write note\n 3) get all notes\n> ";
    write(client_socket, welcome_message, strlen(welcome_message));
    read(client_socket, ch, INPUT_BUF_SIZE);

    if (!strcmp("2\n", ch))
    {
        save(db);
    }

    else if (!strcmp("1\n", ch))
    {
        read_note(db, rc);
    }
    else if (!strcmp("3\n", ch))
    {
        get_notes();
    }

    else
    {
        char* error_message = "Invalid action!\n";
        write(client_socket, error_message, strlen(error_message));
    }

    free(ch);
    close(client_socket);

}

int serve(unsigned int port)
{
    int server_sockfd;
    socklen_t server_len, client_len;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;

    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(port);
    server_len = sizeof(server_address);
    bind(server_sockfd, (struct sockaddr *)&server_address, server_len);

    // Create a connection queue, ignore child exit details and wait for clients
    listen(server_sockfd, 5);
    signal(SIGCHLD, SIG_IGN);

    printf("Server started on port %d\n", port);

    while (TRUE)
    {
        // Accept connection
        client_len = sizeof(client_address);
        client_socket = accept(server_sockfd, (struct sockaddr *)&client_address, &client_len);

        // Fork to create a process for this client and perform a test to see whether we're the parent or the child
        if (fork() == 0)
        {
            dialog();
            exit(0);
        }

        // Otherwise, we must be the parent and our work for this client is finished
        else
        {
            close(client_socket);
        }
    }
}

int main() {
    serve(1337);
    return 0;
}
