#!/bin/sh
export PASS=$(cat /proc/sys/kernel/random/uuid)

sqlite3 notes.db "CREATE TABLE IF NOT EXISTS Notes(Uuid TEXT PRIMARY KEY, Note TEXT);"
sqlite3 notes.db "INSERT INTO Notes(Uuid, Note) VALUES ('$(cat /proc/sys/kernel/random/uuid)', '$FLAG');"

chmod +x /usr/share/app/server
/usr/share/app/server
