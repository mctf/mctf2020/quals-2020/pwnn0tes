#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uuid/uuid.h>


int create_table(sqlite3 *db)
{
    int rc = 0;
    char *error_message = NULL;
    char *sql = "CREATE TABLE IF NOT EXISTS Notes(Uuid TEXT PRIMARY KEY, Note TEXT);";

    rc = sqlite3_exec(db, sql, 0, 0, &error_message);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL Error: %s\n", error_message);
        sqlite3_free(error_message);
    }
    else
    {
        printf("Table created successfully\n");
    }
    return rc;
}

int save_note(sqlite3 *db, char *note, char *inserted_uuid)
{
    sqlite3_stmt *prepared_statement = NULL;
    int rc = 0;
    char *sql = "INSERT INTO Notes(Uuid, Note) VALUES (@uuid, @note)";

    rc = sqlite3_prepare(db, sql, -1, &prepared_statement, 0);

    uuid_t binuuid;
    uuid_generate_random(binuuid);
    uuid_unparse_lower(binuuid, inserted_uuid);

    if (rc == SQLITE_OK)
    {

        int index = sqlite3_bind_parameter_index(prepared_statement, "@uuid");
        sqlite3_bind_text(prepared_statement, index, inserted_uuid, strlen(inserted_uuid), NULL);

        index = sqlite3_bind_parameter_index(prepared_statement, "@note");
        sqlite3_bind_text(prepared_statement, index, note, strlen(note), NULL);
    }
    else
    {
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
        return rc;
    }

    int step = sqlite3_step(prepared_statement);
    if (step != SQLITE_DONE)
    {
        return step;
    }
    sqlite3_finalize(prepared_statement);

    return sqlite3_errcode(db);
}

int get_note(sqlite3 *db, char* uuid, char *buf, size_t buf_size)
{
    sqlite3_stmt *prepared_statement = NULL;
    int rc = 0;
    char *sql = "SELECT Note FROM Notes WHERE Uuid = @uuid";

    rc = sqlite3_prepare(db, sql, -1, &prepared_statement, 0);

    if (rc == SQLITE_OK)
    {
        int index = sqlite3_bind_parameter_index(prepared_statement, "@uuid");
        sqlite3_bind_text(prepared_statement, index, uuid, strlen(uuid), NULL);
    }
    else
    {
        fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
        return rc;
    }

    int step = sqlite3_step(prepared_statement);

    if (step == SQLITE_ROW)
    {
        memcpy(buf, (char *)sqlite3_column_text(prepared_statement, 0), buf_size);
        buf[buf_size] = 0;
    }
    else
    {
        buf = NULL;
        return SQLITE_NOTFOUND;
    }
    sqlite3_finalize(prepared_statement);

    return sqlite3_errcode(db);
}

int get_all_notes(sqlite3 *db, char* buf, size_t buf_size)
{
    sqlite3_stmt *prepared_statement = NULL;
    int rc = 0;
    char *sql = "SELECT group_concat(Note) FROM Notes;";

    rc = sqlite3_prepare(db, sql, -1, &prepared_statement, 0);

    int step = sqlite3_step(prepared_statement);

    if (step == SQLITE_ROW)
    {
        memcpy(buf, (char *)sqlite3_column_text(prepared_statement, 0), buf_size);
        buf[buf_size] = 0;
    }
    else
    {
        buf = NULL;
        return SQLITE_NOTFOUND;
    }
    sqlite3_finalize(prepared_statement);

    return sqlite3_errcode(db);
}
